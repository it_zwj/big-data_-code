package com.czxy

import org.apache.flink.api.scala._

/*
 * @Author: Alice菌
 * @Date: 2020/7/8 08:33
 * @Description: 
    
 */
object BatchWordCount {

  def main(args: Array[String]): Unit = {

    // 1、创建批处理执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    // 2、构建数据源
    // 导入隐式转换
    val wordDataSet: DataSet[String] = env.fromCollection {
      List("hadoop hive spark", "flink mapreduce hadoop hive", "flume spark spark hive") }

    // 3、使用flatMap操作将字符串进行切割后扁平化
    val words: DataSet[String] = wordDataSet.flatMap(_.split(" "))

    // 4、使用 map 操作将单词转换为，（单词，数量）的元组
    val wordNumDataSet: DataSet[(String, Int)] = words.map(_ -> 1)

    // 5、使用 groupBy 操作按照第一个字段进行分组
    val wordGroupDataSet: GroupedDataSet[(String, Int)] = wordNumDataSet.groupBy(0)

    // 6、使用 sum 操作进行分组累加统计
    val wordCountDataSet: AggregateDataSet[(String, Int)] = wordGroupDataSet.sum(1)

    // 打印结果
    //wordCountDataSet.print()
    //(hive,3)
    //(mapreduce,1)
    //(spark,3)
    //(flink,1)
    //(hadoop,2)
    //(flume,1)

    // 将数据源保存到HDFS
    wordCountDataSet.writeAsText("hdfs://node01/output/BatchWordCount")
    // 设置任务名称
    env.execute("BatchWordCount")

  }
}
