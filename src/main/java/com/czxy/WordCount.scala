package com.czxy

import org.apache.flink.api.scala._
/*
 * @Author: Alice菌
 * @Date: 2020/7/7 09:57
 * @Description: 
    
 */
object WordCount {

  def main(args: Array[String]): Unit = {

    // 1、创建批处理执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    // 2、从文件中读取数据
    val inputPath: String = "E:\\2020大数据新学年\\BigData\\Flink\\测试\\hello.txt"

    val inputDS: DataSet[String] = env.readTextFile(inputPath)

    // 分词之后，对单词进行groupby分组，然后用sum进行聚合
    val wordCountDS: AggregateDataSet[(String, Int)] = inputDS.flatMap(_.split(" ")).map((_, 1)).groupBy(0).sum(1)

    // 打印输出
    wordCountDS.print()

  }
}
