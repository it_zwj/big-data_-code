package com.czxy.pengan;

import java.util.Arrays;

/**
 * @Author: Alice菌
 * @Date: 2020/10/14 08:48
 * @Description:
 */
public class demo01 {
    public static void main(String[] args) {


        String s = "aact=a b c aaa=zzz bbb zzz cc=ddd dd=aa bb cc";

        String result = deal01(s);

        System.out.println(result);

    }

    public static String deal01(String s) {
        String[] split = s.split("=");

        // 静态初始化数组，用于存放键
        String[] KeyArray = new String[split.length - 1];
        // 静态初始化数组，用于存放值
        String[] ValueArray = new String[split.length - 1];

        KeyArray[0] = split[0];
        ValueArray[ValueArray.length - 1] = split[split.length - 1];

        for (int i = 1; i < split.length - 1; i++) {
            String[] split1 = split[i].split(" ");
            System.out.println(Arrays.toString(split1));
            // 把按照空格切分后的元素的最后一个值放到存放键的数组
            KeyArray[i] = split1[split1.length - 1];

            // 将除最后一个元素外的其他元素，放到存放值的数组
            for (int j = 0; j < split1.length - 1; j++) {
                ValueArray[i - 1] += split1[j] + " ";
            }
        }

        // 定义一个字符串保存最终的结果
        String result = "";

        for (int i = 0; i < KeyArray.length; i++) {

            if (i==KeyArray.length-1){
                result += KeyArray[i] + "=" +ValueArray[i].replace("null", "");
            }else {
                result += KeyArray[i] + "=" +ValueArray[i].replace("null", "") + "&";
            }
        }

        return result;
    }
}