package com.czxy.pengan

/*
 * @Author: Alice菌
 * @Date: 2020/10/21 10:47
 * @Description: 
    
 */
object QuickSort {

  def main(args: Array[String]): Unit = {

    var dataList: List[Int] = List(1,2,3,4,8,9,10,1,2,3)

    quickSort(dataList)

    println(dataList)

  }

  def quickSort(list: List[Int]): List[Int] = list match {
    case Nil => Nil
    case List() => List()
    case head :: tail =>
      val (left, right) = tail.partition(_ < head)
      quickSort(left) ::: head :: quickSort(right)
  }

}
