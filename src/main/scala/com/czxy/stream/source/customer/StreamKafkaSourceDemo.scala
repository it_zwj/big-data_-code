package com.czxy.stream.source.customer

import java.util.Properties
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.api.scala._
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011

/*
 * @Author: Alice菌
 * @Date: 2020/8/8 22:51
 * @Description:

    基于 kafka 的 source 操作
 */
object StreamKafkaSourceDemo {
  def main(args: Array[String]): Unit = {

    // 1、构建流处理执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    // 指定消费者主题
    val topic: String = "test"
    // 设置参数
    val props: Properties = new Properties
    props.setProperty("bootstrap.servers", "node01:9092")
    props.setProperty("group.id", "test")
    props.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")

    // 基于 Flink，创建 Kafka消费者
    val kafkaConsumer: FlinkKafkaConsumer011[String] = new FlinkKafkaConsumer011[String](topic,new SimpleStringSchema(),props)
    // Flink 从 topic 中最新的数据开始消费【设置消费策略】
    kafkaConsumer.setStartFromLatest()
    // 构建基于 kafka 的数据源
    val kafkaDataStream: DataStream[String] = senv.addSource(kafkaConsumer)
    // 打印输出消费的数据
    kafkaDataStream.print()
    // 执行流处理的程序
    senv.execute("StreamKafkaSourceDemo")


  }
}
