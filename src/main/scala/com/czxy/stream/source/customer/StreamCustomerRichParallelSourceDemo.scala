package com.czxy.stream.source.customer


import org.apache.flink.streaming.api.functions.source.{RichParallelSourceFunction,SourceFunction}
import org.apache.flink.streaming.api.scala.{DataStream,StreamExecutionEnvironment}
import org.apache.flink.api.scala._
import org.apache.flink.configuration.Configuration

/*
 * @Author: Alice菌
 * @Date: 2020/8/8 22:23
 * @Description:
    创建并行数据源
 */
object StreamCustomerRichParallelSourceDemo {
  def main(args: Array[String]): Unit = {
    // 1、 创建流处理运行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    // 2、 基于 RichParallelSource 并行数据源构建数据集
    val richParallelSource: DataStream[Long] = senv.addSource(new MyRichParallelSource()).setParallelism(2)

    // 3、 打印输出
    richParallelSource.map(line => {
      println("接收到的数据:" + line)
      line
    })

    // 4、执行程序
    senv.execute("StreamCustomerRichParallelSourceDemo")

  }
  /*
     创建一个并行度为1 的数据源
     实现从 1 开始产生递增数字
   */

  class MyRichParallelSource extends RichParallelSourceFunction[Long] {

    var count: Long = 1L
    var isRunning: Boolean = true

    override def run(ctx: SourceFunction.SourceContext[Long]): Unit = {

      while (isRunning){
        ctx.collect(count)
        count += 1
        Thread.sleep(1000)

      }
    }

    override def cancel(): Unit = {
      isRunning = false
    }

    override def open(parameters: Configuration): Unit = {
      super.close()
    }

  }
}
