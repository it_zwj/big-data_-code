package com.czxy.stream.source.customer


import org.apache.flink.streaming.api.functions.source.{ParallelSourceFunction, SourceFunction}
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.scala._
/*
 * @Author: Alice菌
 * @Date: 2020/8/8 22:05
 * @Description: 

    自定义创建并行数据源
 */
object StreamCustomerParallelSourceDemo {
  def main(args: Array[String]): Unit = {

    // 1、创建流处理的执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    // 2、基于自定义ParallelSource数据源创建并行的数据
    val parallelSource: DataStream[Long] = senv.addSource(new MyParallelSource()).setParallelism(5)

    // 3、打印输出
    parallelSource.print()

    // 4、执行程序
    senv.execute("StreamCustomerParallelSourceDemo")

  }

  /*
  创建一个并行度为1的数据源 * 实现从1开始产生递增数字
   */
  class MyParallelSource extends ParallelSourceFunction[Long] {
    // 声明一个Long类型的变量
    var number:Long = 1L
    // 申明一个初始化为true的Boolean变量
    var isRunning: Boolean = true

    override def run(ctx: SourceFunction.SourceContext[Long]): Unit = {

      while (isRunning) {
        ctx.collect(number)
        number += 1
        if (number > 20) {
          cancel()
        }
      }
    }
    override def cancel(): Unit = {
      isRunning = false
    }
  }

}
