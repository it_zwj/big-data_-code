package com.czxy.stream.source.customer

import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet}
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.source.{RichSourceFunction, SourceFunction}
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}

/*
 * @Author: Alice菌
 * @Date: 2020/8/8 23:52
 * @Description: 

    基于mysql的source操作
 */
object StreamFromMysqlSource {
  def main(args: Array[String]): Unit = {

    // 1、创建流处理执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    import org.apache.flink.api.scala._
    
    // 2、添加自定义的 mysql 数据源对象
    val studentDataStream: DataStream[Student] = senv.addSource(new MysqlSource())
    studentDataStream.print()

    senv.execute("StreamFromMysqlSource")

  }

  // 3、创建mysql自定义数据源对象
  class MysqlSource extends RichSourceFunction[Student](){

    // 3.1 声明Connection对象
    var connection:Connection = _
    // 3.2 声明PreparedStatement对象
    var ps: PreparedStatement = _

    // 在 open 方法中进行配置链接信息 drive  url username password
    // 加载驱动 Class.forName(),DriveManager 获取链接，调用prepareStatement,预编译执行sql
    override def open(parameters: Configuration): Unit = {

      val driver: String = "com.mysql.jdbc.Driver"
      val url: String = "jdbc:mysql://localhost:3306/blogs"
      val username: String = "root"
      val password: String = "root"

      Class.forName(driver)
      connection = DriverManager.getConnection(url,username,password)

      val sql: String =
        """
          |select nid,ntitle,content from notice
        """.stripMargin

      ps = connection.prepareStatement(sql)

    }

    // 在run方法中进行查询，结果封装成样例类，ctx进行collect
    override def run(ctx: SourceFunction.SourceContext[Student]): Unit = {

      // 执行 SQL 查询
      val queryResultSet: ResultSet = ps.executeQuery()

      while (queryResultSet.next()){

        // 分别获取到查询的值
        val nid: Int = queryResultSet.getInt("nid")
        val ntitle: String = queryResultSet.getString("ntitle")
        val content: String = queryResultSet.getString("content")

        // 将获取到的值，封装成样例类
        val student: Student = Student(nid,ntitle,content)
        ctx.collect(student)

      }
    }


    override def close(): Unit = {
      if (connection != null){
        connection.close()
      }
      if (ps != null){
        ps.close()
      }

    }

    override def cancel(): Unit = {
    }
  }



  case class Student(nid: Int, ntitle: String, content: String) {
    override def toString: String = {
      "文章id:" + nid + " 标题:" + ntitle + " 内容:" + content }
  }

}
