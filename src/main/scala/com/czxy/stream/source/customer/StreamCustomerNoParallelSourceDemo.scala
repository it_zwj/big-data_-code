package com.czxy.stream.source.customer


import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.scala._

/*
 * @Author: Alice菌
 * @Date: 2020/8/8 21:51
 * @Description: 
    自定义非并行数据源
 */
object StreamCustomerNoParallelSourceDemo {
  def main(args: Array[String]): Unit = {

    // 1、创建流处理的执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 2、基于自定义数据源构建数据
    val longDStream: DataStream[Long] = senv.addSource(new MyNoParallelSource()).setParallelism(1)
    // 3、输出打印
    longDStream.print()
    // 4、执行程序
    senv.execute("StreamCustomerNoParallelSourceDemo")

    //10> 1
    //11> 2
    //12> 3
    //1> 4
    //2> 5
    //3> 6
    //4> 7
    //5> 8
    //6> 9

  }

  /*
  创建一个并行度为1的数据源 * 实现从1开始产生递增数字
  */
  class MyNoParallelSource extends SourceFunction[Long]{
    // 申明一个变量number
    var number:Long = 1L
    var isRunning:Boolean = true

    override def run(ctx: SourceFunction.SourceContext[Long]): Unit = {

      while (isRunning){
        ctx.collect(number)
        number += 1
        // 休眠1秒
        Thread.sleep(1000)
        if (number == 10){
          cancel()
        }
      }
    }

    override def cancel(): Unit = {
      isRunning = false
    }
  }


}
