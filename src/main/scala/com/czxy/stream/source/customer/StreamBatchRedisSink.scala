package com.czxy.stream.source.customer

import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.streaming.connectors.redis.RedisSink
import org.apache.flink.streaming.connectors.redis.common.config.FlinkJedisPoolConfig
import org.apache.flink.streaming.connectors.redis.common.mapper.{RedisCommand, RedisCommandDescription, RedisMapper}

/*
 * @Author: Alice菌
 * @Date: 2020/10/24 00:45
 * @Description: 
    
 */

// 温度传感器读取样例类
case class SensorReading(id:String,timestamp:Long,temperature:Double)

object BatchRedisSink {
  def main(args: Array[String]): Unit = {

     val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

     // 设置并行度
     env.setParallelism(1)

     // source
    val inputStream: DataStream[String] = env.readTextFile("data/input/wordcount_redis.txt")

    // transform
    import org.apache.flink.api.scala._
    // 将文本文件中的数据进行处理，封装至样例类中
    val dataStream: DataStream[SensorReading] = inputStream.map(x => {
      val arr: Array[String] = x.split(",")
      SensorReading(arr(0).trim, arr(1).toLong, arr(2).trim.toDouble)
    })

    // sink
    val conf: FlinkJedisPoolConfig = new FlinkJedisPoolConfig.Builder().setHost("localhost").setPort(6379).build()

    dataStream.addSink(new RedisSink[SensorReading](conf,new MyRedisMapper))

    env.execute("redis sink test")

  }
}

// 定义一个 redis 的 mapper 类，用于定义保存到 redis 时调用的命令
class MyRedisMapper extends RedisMapper[SensorReading]{

  override def getCommandDescription: RedisCommandDescription = {
    // 把传感器id 和 温度值 保存成 哈希表:  HSET key field value
    new RedisCommandDescription(RedisCommand.HSET,"sensor_temperature")
  }

    // 相当于是 field
  override def getKeyFromData(data: SensorReading): String = {
    data.id
  }

  override def getValueFromData(data: SensorReading): String = {
    data.temperature.toString
  }
}
