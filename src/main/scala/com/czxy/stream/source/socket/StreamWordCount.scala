package com.czxy.stream.source.socket

import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow

/*
 * @Author: Alice菌
 * @Date: 2020/7/9 08:40
 * @Description: 
    基于网络套接字的 source
 */
// 入门案例，单词统计
object StreamWordCount {
  def main(args: Array[String]): Unit = {
    // 1、 创建流处理的执行环境
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 2、 构建数据源，使用的socket
    val socketDataStream: DataStream[String] = env.socketTextStream("node01",9999, 0)
    // 3、 数据的处理
    val wordDataStream: DataStream[(String, Int)] = socketDataStream.flatMap(_.split(" ")).map(_ -> 1)
    //4. 使用keyBy 进行分流（分组）
    // 在批处理中针对于dataset， 如果分组需要使用groupby
    // 在流处理中针对于datastream， 如果分组（分流）使用keyBy
    val groupedDataStream: KeyedStream[(String, Int), Tuple] = wordDataStream.keyBy(0)
    //5. 使用timeWinodw 指定窗口的长度（每5秒计算一次）
    // spark-》reduceBykeyAndWindow
    val windowDataStream: WindowedStream[(String,Int),Tuple,TimeWindow]= groupedDataStream.timeWindow(
      Time.seconds(5)
    )


    //6. 使用sum执行累加
    val sumDataStream: DataStream[(String, Int)] = windowDataStream.sum(1)
    sumDataStream.print()

    env.execute("StreamWordCount")

  }
}
