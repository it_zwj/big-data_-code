package com.czxy.stream.source.file

import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}

/*
 * @Author: Alice菌
 * @Date: 2020/8/8 17:42
 * @Description:
    基于文件的source
 */
object StreamFileSourceDemo {

  def main(args: Array[String]): Unit = {
    // 1、构建流处理的环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    // 2、基于文件的source，构建数据集
    val textDStream: DataStream[String] = senv.readTextFile("data/input/wordcount.txt")

    // 3、打印输出
    textDStream.print()

    // 4、执行程序
    senv.execute("StreamFileSourceDemo")

    //3> Final Memory Finished at
    //10> Total time BUILD SUCCESS
    //4> Flink Flink Flink Flink Flink
    //9> Final Memory Finished at
    //1> Total time BUILD SUCCESS
    //8> Total time BUILD SUCCESS
    //12> Final Memory Finished at
    //6> Hive Hive Hive Hive Hive

  }
}
