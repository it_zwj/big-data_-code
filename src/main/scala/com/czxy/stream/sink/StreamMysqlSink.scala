package com.czxy.stream.sink

import java.sql.{Connection, DriverManager, PreparedStatement}

import org.apache.flink.api.scala._
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
/*
 * @Author: Alice菌
 * @Date: 2020/8/10 10:29
 * @Description: 
    
 */
object StreamMysqlSink {

  // 定义一个样例类，用于封装数据
  case class Student(id:Int,name:String,addr:String,sex:String)

  def main(args: Array[String]): Unit = {

    // 1、创建执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 2、准备数据
    val studentDStream: DataStream[Student] = senv.fromElements(
      Student(4, "小明", "上海", "男"),
      Student(5, "小青", "广州", "女"),
      Student(6, "小月", "深圳", "女")
    )

    studentDStream.addSink(new StudentSlinkToMySql)
    senv.execute("StreamMysqlSink")

  }

  class StudentSlinkToMySql extends RichSinkFunction[Student]{

    private var connection:Connection = _
    private var ps:PreparedStatement = _

    override def open(parameters: Configuration): Unit = {

      // 设置驱动，连接地址，用户名，密码
      var driver: String = "com.mysql.jdbc.Driver"
      var url:String = "jdbc:mysql://localhost:3306/blogs?characterEncoding=utf-8&useSSL=false"
      var username: String = "root"
      var password: String = "root"

      // 1、加载驱动
      Class.forName(driver)
      // 2、创建连接
      connection = DriverManager.getConnection(url,username,password)
      // 书写SQL语句
      val sql: String = "insert into student(id,name,addr,sex) values(?,?,?,?);"
      // 3、获得执行语句
      ps = connection.prepareStatement(sql)

    }

    // 关闭连接操作
    override def close(): Unit = {
      if (connection != null){
        connection.close()
      }
      if (ps != null){
        ps.close()
      }
    }

    // 每个元素的插入，都要触发一次 invoke，这里主要进行 invoke 插入
    override def invoke(stu: Student): Unit = {
      try{
        // 4、组装数据，执行插入操作
        ps.setInt(1,stu.id)
        ps.setString(2,stu.name)
        ps.setString(3,stu.addr)
        ps.setString(4,stu.sex)
        ps.executeUpdate()
      } catch {
        case e:Exception => println(e.getMessage)
      }
    }
  }


}
