package com.czxy.stream.sink

import java.util.Properties
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011


/*
 * @Author: Alice菌
 * @Date: 2020/8/10 10:08
 * @Description: 

    sink 到 kafka
 */
object StreamKafkaSink {
  def main(args: Array[String]): Unit = {

    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    // 导入隐式转换
    import org.apache.flink.api.scala._
    val source: DataStream[String] = senv.fromElements("1,小丽,北京,女")

    val properties: Properties = new Properties()
    properties.setProperty("bootstrap.servers","node01:9092")

    // 获取Flink 连接 Producer接口
    val flinkKafkaProducer: FlinkKafkaProducer011[String] = new FlinkKafkaProducer011[String]("title",new SimpleStringSchema(),properties)

    source.addSink(flinkKafkaProducer)

    // 打印
    source.print()

    // 执行
    senv.execute("StreamKafkaSink")


  }
}
