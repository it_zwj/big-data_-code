package com.czxy.stream.transformation

import org.apache.flink.streaming.api.scala.{DataStream, SplitStream, StreamExecutionEnvironment}

/*
 * @Author: Alice菌
 * @Date: 2020/7/9 11:38
 * @Description: 
    
 */
object StreamSplit {
  def main(args: Array[String]): Unit = {

    // 1、创建执行环境
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 2、构建数据集
    import org.apache.flink.api.scala._
    val source: DataStream[Int] = env.fromElements(1,2,3,4,5,6,7,8,9)

    val spiltStream: SplitStream[Int] = source.split(x => {
      x % 2 match {
        case 0 => List("偶数")
        case 1 => List("奇数")
      }
    })

    val selectDataStream: DataStream[Int] = spiltStream.select("奇数")

    selectDataStream.print()

    env.execute("StreamSplit")

    //2> 7
    //3> 9
    //11> 1
    //12> 3
    //1> 5

  }
}
