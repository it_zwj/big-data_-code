package com.czxy.stream.transformation

import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala.{ConnectedStreams, DataStream, StreamExecutionEnvironment}
import org.apache.flink.api.scala._
/*
 * @Author: Alice菌
 * @Date: 2020/8/10 15:48
 * @Description: 
    Connect：
    用来将两个 dataStream 组装成一个 ConnectedStreams
    而且这个 connectedStream 的组成结构就是保留原有的 dataStream 的结构体；
    这样我们 就可以把不同的数据组装成同一个结构
 */
object StreamConnectDemo {
  def main(args: Array[String]): Unit = {

    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    val source1: DataStream[Long] = senv.addSource(new MyNoParallelSource)
    val source2: DataStream[Long] = senv.addSource(new MyNoParallelSource)

    val connectStreams: ConnectedStreams[Long, Long] = source1.connect(source2)

    val result: DataStream[String] = connectStreams.map(
      function1 => {
        "function1 = " + function1
      },
      function2 => {
        "function2 = " + function2
      }
    )

    result.print()

    senv.execute("StreamConnectDemo")

    //8> function2 = 1
    //2> function1 = 1
    //3> function1 = 2
    //9> function2 = 2
    //4> function1 = 3
    //10> function2 = 3
    //5> function1 = 4
    //11> function2 = 4
    //12> function2 = 5
    //6> function1 = 5

  }

  /**
    * 创建自定义并行度为 1 的 source
    * 实现从 1 开始产生递增数字
    */
  class MyNoParallelSource extends SourceFunction[Long]{

    var count:Long = 1L
    var isRunning: Boolean = true

    override def run(ctx: SourceFunction.SourceContext[Long]): Unit = {

      while (isRunning){
        ctx.collect(count)
        count += 1
        Thread.sleep(1000)
        if (count > 5){
          cancel()
        }
      }
    }

    override def cancel(): Unit = {
      isRunning = false
    }
  }
}
