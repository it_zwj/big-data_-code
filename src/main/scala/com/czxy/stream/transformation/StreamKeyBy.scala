package com.czxy.stream.transformation

import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.scala.{DataStream, KeyedStream, StreamExecutionEnvironment}

/*
 * @Author: Alice菌
 * @Date: 2020/7/9 11:25
 * @Description: 
    
 */
// keyBy   分组操作算子
object StreamKeyBy {
  def main(args: Array[String]): Unit = {

    // 1、创建执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 2、构建数据集
    import org.apache.flink.api.scala._
    val elementSource: DataStream[String] = senv.fromElements("hadoop hadoop scala")
    // 3、数据组成元组类型
    val wordAndOne: DataStream[(String, Int)] = elementSource.flatMap(x=>x.split(" ")).map((_,1))
    // 4、进行分组
    val KeyedStream: KeyedStream[(String, Int), Tuple] = wordAndOne.keyBy(0)
    // 5、聚合计算
    val result: DataStream[(String, Int)] = KeyedStream.sum(1)
    // 6、打印输出
    result.print()
    // 7、执行程序
    senv.execute("StreamKeyBy")
    //1> (scala,1)
    //11> (hadoop,1)
    //11> (hadoop,2)


  }
}
