package com.czxy

import org.apache.flink.api.common.JobExecutionResult
import org.apache.flink.api.common.accumulators.IntCounter
import org.apache.flink.api.common.functions.RichMapFunction
import org.apache.flink.api.scala._
import org.apache.flink.configuration.Configuration

/*
 * @Author: Alice菌
 * @Date: 2020/7/9 08:14
 * @Description: 
    
 */
object BatchCounterDemo {
  def main(args: Array[String]): Unit = {
    // 1、创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 2、构建数据集
    val sourceDataSet: DataSet[String] = env.fromElements("a", "b", "c", "d")
    // 3、数据处理
    val resultDataSet: DataSet[String] = sourceDataSet.map(new RichMapFunction[String, String] {
      // 1) 创建累加器
      val counter: IntCounter = new IntCounter()

      override def open(parameters: Configuration): Unit = {
        // 2) 注册累加器
        getRuntimeContext.addAccumulator("myAccumulator", counter)
      }

      // 每条数据都会执行一次
      override def map(value: String): String = {
        // 3) 使用累加器
        counter.add(1)
        value
      }
    })


    //val result: JobExecutionResult = env.execute("BatchCounterDemo")
    //val MyAccumulatorValue: Int = result.getAccumulatorResult[Int]("myAccumulator")

    print("累加器的值"+resultDataSet.print)

  }
}
