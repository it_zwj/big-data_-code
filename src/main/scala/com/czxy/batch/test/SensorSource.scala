package com.czxy.batch.test

import com.czxy.stream.source.customer.SensorReading
import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.streaming.api.functions.source.SourceFunction

import scala.collection.immutable
import scala.util.Random
import org.apache.flink.api.scala._
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
/*
 * @Author: Alice菌
 * @Date: 2020/10/24 11:35
 * @Description: 

    Flink 自定义数据源 Source

 */
object SensorSource {
  def main(args: Array[String]): Unit = {

    // 1、创建执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    // 2、创建并行数据源
    val parallelSource: DataStream[SensorReading] = senv.addSource(new MySensorSource()).setParallelism(1)

    // 3、打印输出
    parallelSource.print()

    // 4、执行程序
    senv.execute("StreamCustomerParallelSourceDemo")

  }

  /**
    * SourceFunction是Flink中所有流数据源的基本接口
    *
    * SourceFunction定义了run和cancel两个方法和SourceContext内部接口。
    *
    * run(SourceContex)：实现数据获取逻辑，并可以通过传入的参数ctx进行向下游节点的数据转发。
    * cancel()：用来取消数据源，一般在run方法中，会存在一个循环来持续产生数据，cancel方法则可以使该循环终止。
    * SourceContext：source函数用于发出元素和可能的watermark的接口，返回source生成的元素的类型。
    *
    */
  class MySensorSource extends SourceFunction[SensorReading]{

    // 声明一个Long类型的变量
    var number:Long = 1L

    // flag: 表现数据源是否还在正常运行
    var running:Boolean = true

    override def run(ctx: SourceFunction.SourceContext[SensorReading]): Unit = {

      // 初始化一个随机数发生器
      val rand: Random = new  Random()

      var curTemp: immutable.IndexedSeq[(String, Double)] = 1.to(10).map(
        i => ( "sensor_" + i, 65 + rand.nextGaussian() * 20 )
      )

      while (running){
        // 更新温度值
        curTemp.map(

          t => (t._1,t._2 + rand.nextGaussian())
        )
        // 获取当前时间戳
        val curTime: Long = System.currentTimeMillis()

        curTemp.foreach(
          t => ctx.collect(SensorReading(t._1,curTime,t._2))
        )

        number += 1

        Thread.sleep(100)

        // 添加个停止条件
        if (number > 5){
          cancel()
        }
      }
    }
    override def cancel(): Unit = {
         running = false
    }
  }
}
