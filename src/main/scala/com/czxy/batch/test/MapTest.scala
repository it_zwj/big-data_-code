package com.czxy.batch.test

import org.apache.flink.api.scala.{DataSet, ExecutionEnvironment}

import org.apache.flink.api.scala._

/*
 * @Author: Alice菌
 * @Date: 2020/9/6 10:18
 * @Description: 
    
 */
object MapTest {
  def main(args: Array[String]): Unit = {

    //1、创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    //2、创建执行环境
    val sourceDataSet: DataSet[String] = env.fromElements("a","b","c","d")

    val results: DataSet[String] = sourceDataSet.map(getAnotherString(_))

    results.print()

  }

  def getAnotherString(str:String): String ={
    str + "-"
  }
}
