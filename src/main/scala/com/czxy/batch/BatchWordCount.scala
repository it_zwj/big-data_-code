package com.czxy.batch

import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.api.scala._
/*
 * @Author: Alice菌
 * @Date: 2020/7/20 15:55
 * @Description: 

    编写Flink程序，统计单词
 */
object BatchWordCount {
  def main(args: Array[String]): Unit = {

    // 1、创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 2、接入数据源
    val testDataSet: DataSet[String] = env.fromCollection(List("hadoop spark hive","hadoop hadoop spark"))
    // 3、进行数据处理
    // 切分
    val wordDataSet: DataSet[String] = testDataSet.flatMap(_.split(" "))
    // 每个单词标记1
    val wordAndOneDataSet: DataSet[(String, Int)] = wordDataSet.map((_,1))
    // 按照单词进行分组
    val groupDataSet: GroupedDataSet[(String, Int)] = wordAndOneDataSet.groupBy(0)
    // 对单词进行聚合
    val sumDataSet: AggregateDataSet[(String, Int)] = groupDataSet.sum(1)
    // 4、数据保存或输出
    sumDataSet.writeAsText("./ResultData/BatchWordCount")

    //sumDataSet.print()
    env.execute("BatchWordCount")

  }
}
