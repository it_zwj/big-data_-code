package com.czxy.batch

import org.apache.flink.api.common.JobExecutionResult
import org.apache.flink.api.common.accumulators.IntCounter
import org.apache.flink.api.common.functions.RichMapFunction
import org.apache.flink.api.scala._
import org.apache.flink.configuration.Configuration
import org.apache.flink.core.fs.FileSystem.WriteMode


/*
 * @Author: Alice菌
 * @Date: 2020/8/1 23:26
 * @Description: 
    累加器
 */
object BatchCounterDemo {
  def main(args: Array[String]): Unit = {

    //1、创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    //2、创建执行环境
    val sourceDataSet: DataSet[String] = env.fromElements("a","b","c","d")

    //3、对sourceDataSet 进行map操作
    val resultDataSet: DataSet[String] = sourceDataSet.map(new RichMapFunction[String, String] {
      // 创建累加器
      val counter: IntCounter = new IntCounter

      // 初始化的时候执行一次
      override def open(parameters: Configuration): Unit = {
        // 注册累加器
        getRuntimeContext.addAccumulator("MyAccumulator", this.counter)
      }

      // 初始化的时候被执行一次
      override def map(value: String): String = {
        counter.add(1)
        value
      }
    })

    resultDataSet.writeAsText("data/output/Accumulators",WriteMode.OVERWRITE)
    val result: JobExecutionResult = env.execute("BatchCounterDemo")

    val MyAccumlatorValue: Int = result.getAccumulatorResult[Int]("MyAccumulator")

    println("累加器的值:"+MyAccumlatorValue)
    //累加器的值:4
  }
}
