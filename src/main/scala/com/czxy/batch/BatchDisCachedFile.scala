package com.czxy.batch

import java.io.File

import org.apache.flink.api.common.functions.RichMapFunction
import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.configuration.Configuration
import org.apache.flink.api.scala._

import scala.io.Source
/*
 * @Author: Alice菌
 * @Date: 2020/8/1 22:40
 * @Description:
 *     分布式缓存
    
 */
object BatchDisCachedFile {
  def main(args: Array[String]): Unit = {

    // 获取批处理运行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    // 注册一个分布式缓存
    env.registerCachedFile("hdfs://node01:8020/test/input/distribute_cache_student","student")
    /*
    1,张三
    2,李四
    3,王五
     */
    // 创建成绩数据集
    val scoreDataSet: DataSet[(Int, String, Int)] = env.fromCollection(List((1, "语文", 50), (2, "数学", 70), (3, "英文", 86)))

    val resultDataSet: DataSet[(String, String, Int)] = scoreDataSet.map(
      new RichMapFunction[(Int, String, Int), (String, String, Int)] {

        var studentMap: Map[Int, String] = _

        // 初始化的时候被调用一次
        override def open(parameters: Configuration): Unit = {
          // 获取分布式缓存的文件
          val studentFile: File = getRuntimeContext.getDistributedCache.getFile("student")

          val linesIter: Iterator[String] = Source.fromFile(studentFile).getLines()
          studentMap = linesIter.map(lines => {
            val words: Array[String] = lines.split(",")
            (words(0).toInt, words(1))
          }).toMap
        }

        override def map(value: (Int, String, Int)): (String, String, Int) = {

          val stuName: String = studentMap.getOrElse(value._1, "")
          (stuName, value._2, value._3)
        }
      })

    // 输出结果
    resultDataSet.print()
    //(张三,语文,50)
    //(李四,数学,70)
    //(王五,英文,86)

  }
}
