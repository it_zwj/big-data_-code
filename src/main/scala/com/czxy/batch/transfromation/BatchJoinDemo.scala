package com.czxy.batch.transfromation

import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.api.scala._

/*
 * @Author: Alice菌
 * @Date: 2020/7/28 14:39
 * @Description: 
    
 */
object BatchJoinDemo {

  case class Subject(id:Int,name:String)

  case class Score(id:Int,stuName:String,subId:Int,score:Double)

  def main(args: Array[String]): Unit = {

    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    val subjectDataSet: DataSet[Score] = env.readCsvFile("E:\\2020大数据新学年\\BigData\\06-Flink\\课堂资料\\0708\\day02资料\\测试数据源\\score.csv")

    val scoreDataSet: DataSet[Subject] = env.readCsvFile("E:\\2020大数据新学年\\BigData\\06-Flink\\课堂资料\\0708\\day02资料\\测试数据源\\subject.csv")

    val joinDataSet: JoinDataSet[Score, Subject] = subjectDataSet.join(scoreDataSet).where(_.subId).equalTo(_.id)

    joinDataSet.print()

    //(Score(27,小七,3,78.0),Subject(3,英语))
    //(Score(15,王五,3,58.0),Subject(3,英语))
    //(Score(21,赵六,3,65.0),Subject(3,英语))
    //(Score(3,张三,3,89.0),Subject(3,英语))
    //(Score(9,李四,3,65.0),Subject(3,英语))
    //(Score(17,王五,5,78.0),Subject(5,化学))
    //(Score(29,小七,5,65.0),Subject(5,化学))
    //(Score(23,赵六,5,70.0),Subject(5,化学))
    //(Score(5,张三,5,78.0),Subject(5,化学))
    //(Score(11,李四,5,70.0),Subject(5,化学))
    //(Score(18,王五,6,98.0),Subject(6,生物))
    //(Score(30,小七,6,78.0),Subject(6,生物))
    //(Score(12,李四,6,78.0),Subject(6,生物))
    //(Score(24,赵六,6,78.0),Subject(6,生物))
    //(Score(6,张三,6,70.0),Subject(6,生物))
    //(Score(19,赵六,1,77.5),Subject(1,语文))
    //(Score(7,李四,1,78.0),Subject(1,语文))
    //(Score(13,王五,1,70.0),Subject(1,语文))
    //(Score(1,张三,1,98.0),Subject(1,语文))
    //(Score(25,小七,1,78.0),Subject(1,语文))
    //(Score(28,小七,4,58.0),Subject(4,物理))
    //(Score(16,王五,4,65.0),Subject(4,物理))
    //(Score(22,赵六,4,78.0),Subject(4,物理))
    //(Score(4,张三,4,65.0),Subject(4,物理))
    //(Score(10,李四,4,78.0),Subject(4,物理))
    //(Score(14,王五,2,78.0),Subject(2,数学))
    //(Score(20,赵六,2,89.0),Subject(2,数学))
    //(Score(8,李四,2,58.0),Subject(2,数学))
    //(Score(2,张三,2,77.5),Subject(2,数学))
    //(Score(26,小七,2,70.0),Subject(2,数学))
  }
}
