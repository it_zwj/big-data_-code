package com.czxy.batch.transfromation

import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.api.scala._

import scala.collection.mutable.ListBuffer
/*
 * @Author: Alice菌
 * @Date: 2020/7/28 14:58
 * @Description: 
    左外连接,左边的Dataset中的每一个元素，去连接右边的元素
 */

object BatchLeftOuterJoinDemo {
  def main(args: Array[String]): Unit = {

    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    val data1: ListBuffer[(Int, String)] = ListBuffer[Tuple2[Int,String]]()

    data1.append((1,"zhangsan"))
    data1.append((2,"lisi"))
    data1.append((3,"wangwu"))
    data1.append((4,"zhaoliu"))

    val data2: ListBuffer[(Int, String)] = ListBuffer[Tuple2[Int,String]]()
    data2.append((1,"beijing"))
    data2.append((2,"shanghai"))
    data2.append((4,"guangzhou"))

    val text1: DataSet[(Int, String)] = env.fromCollection(data1)
    val text2: DataSet[(Int, String)] = env.fromCollection(data2)

    text1.leftOuterJoin(text2).where(0).equalTo(0).apply(
      (first,second) =>{
        if (second == null){
          (first._1,first._2,"null")
        }else{
          (first._1,first._2,second._2)
        }
      }).print()

    //(3,wangwu,null)
    //(1,zhangsan,beijing)
    //(4,zhaoliu,guangzhou)
    //(2,lisi,shanghai)

  }
}
