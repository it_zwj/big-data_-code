package com.czxy.batch.transfromation

import org.apache.flink.api.scala._

/*
 * @Author: Alice菌
 * @Date: 2020/7/26 23:29
 * @Description: 
    
 */
object BatchReduceGroupDemo {
  def main(args: Array[String]): Unit = {

     val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    val textDataSet: DataSet[(String, Int)] = env.fromCollection(List(("java" , 1),("java", 1),("scala" , 1)))

    val groupedDataSet: GroupedDataSet[(String, Int)] = textDataSet.groupBy(0)

    val reduceGroupDataSet: DataSet[(String, Int)] = groupedDataSet.reduceGroup(group => {

      group.reduce((v1, v2) => {
        (v1._1, v1._2 + v2._2)
      })
    })
    reduceGroupDataSet.print()

    //(java,2)
    //(scala,1)
  }
}
