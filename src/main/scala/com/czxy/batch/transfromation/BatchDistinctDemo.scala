package com.czxy.batch.transfromation

import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.api.scala._

/*
 * @Author: Alice菌
 * @Date: 2020/7/28 13:58
 * @Description: 
    
 */
object BatchDistinctDemo {
  def main(args: Array[String]): Unit = {

    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    val textDataSet: DataSet[(String, Int)] = env.fromCollection(List(("java",1),("java",1),("scala",1)))

    textDataSet.distinct(0).print()

    //(java,1)
    //(scala,1)

  }
}
