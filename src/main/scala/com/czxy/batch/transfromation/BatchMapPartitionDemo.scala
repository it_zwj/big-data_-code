package com.czxy.batch.transfromation


import org.apache.flink.api.scala._

/*
 * @Author: Alice菌
 * @Date: 2020/7/8 20:55
 * @Description: 
    
 */

object BatchMapPartitionDemo {

  case class user(id:Int,name:String)

  def main(args: Array[String]): Unit = {
    // 1、创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 2、构建数据集
    val sourceDataSet: DataSet[String] = env.fromElements("1,张三", "2,李四", "3,王五", "4,赵六")
    // 3、数据处理
    val userDataSet: DataSet[user] = sourceDataSet.mapPartition(itemPartition => {

      itemPartition.map(item => {

        val itemsArr: Array[String] = item.split(",")
        user(itemsArr(0).toInt, itemsArr(1))

      })
    })

    // 4、打印数据
    userDataSet.print()
    //user(1,张三)
    //user(2,李四)
    //user(3,王五)
    //user(4,赵六)

  }
}
