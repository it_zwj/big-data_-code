package com.czxy.batch.transfromation

import org.apache.flink.api.common.functions.RichMapFunction
import org.apache.flink.api.scala._
/*
 * @Author: Alice菌
 * @Date: 2020/7/8 21:00
 * @Description: 
    
 */

object BatchRebalanceDemo {

  def main(args: Array[String]): Unit = {

    // 1、构建批处理运行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 2、使用 env.generateSequence 创建 0 - 100 的并行数据
    val source: DataSet[Long] = env.generateSequence(0,100)
    // 3、 使用 filter 过滤出来 大于 8 的数字
    val filter: DataSet[Long] = source.filter(_>8)
    // 使用 rebalance  进行处理数据【防止数据倾斜】
    val rebalance: DataSet[Long] = filter.rebalance()
    // 4、使用 map 操作传入 RichMapFunction ， 将当前子任务的 ID 和数字构建成一个元组
    val result: DataSet[(Int, Long)] = rebalance.map(new RichMapFunction[Long, (Int, Long)] {
      override def map(value: Long): (Int, Long) = {
        (getRuntimeContext.getIndexOfThisSubtask,value)
      }
    })

    result.print()
    // 结果跟核数有关
  }
}
