package com.czxy.batch.transfromation
import org.apache.flink.api.scala._

import scala.collection.mutable
/*
 * @Author: Alice菌
 * @Date: 2020/7/8 10:23
 * @Description: 
    1) 构建批处理运行环境
    2) 构建本地集合数据源
    3) 使用 flatMap 将一条数据转换为三条数据 a. 使用逗号分隔字段 b. 分别构建国家、国家省份、国家省份城市三个元组
    4) 打印输出
 */
object BashFlatMapDemo {

  def main(args: Array[String]): Unit = {

    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    val userDataSet: DataSet[String] = env.fromCollection(List("张三,中国,江西省,南昌市","李四,中国,河北省,石家庄市", "Tom,America,NewYork,Manhattan"))

    val userAdressSetData: DataSet[(String, String)] = userDataSet.flatMap(item => {
      val field: mutable.ArrayOps[String] = item.split(",")

      List((field(0), field(1)),
        (field(0), field(1) + field(2)),
        (field(0), field(1) + field(2) + field(3)))
    })

    // 输出结果
    userAdressSetData.print()

    //(张三, 中国)
    //(张三, 中国江西省)
    //(张三, 中国江西省南昌市)

    //(李四,中国)
    //(李四,中国河北省)
    //(李四,中国河北省石家庄市)

    //(Tom,America)
    //(Tom,AmericaNewYork)
    //(Tom,AmericaNewYorkManhattan)

  }
}
