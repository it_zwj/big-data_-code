package com.czxy.batch.transfromation


import org.apache.flink.api.scala._

/*
 * @Author: Alice菌
 * @Date: 2020/7/29 15:42
 * @Description: 
    
 */
object BatchUnionDemo {
  def main(args: Array[String]): Unit = {

    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    // 使用 fromCollection 创建两个数据源
    val wordDataSet1: DataSet[String] = env.fromCollection(List("hadoop","hive","flume"))
    val wordDataSet2: DataSet[String] = env.fromCollection(List("hadoop","hive","spark"))

    val wordDataSet3: DataSet[String] = env.fromElements("hadoop")
    val wordDataSet4: DataSet[String] = env.fromElements("hadoop")

    wordDataSet1.union(wordDataSet2).print()
    println("- - - - - - - - - - - - - - - - - - - - - - - - -")
    wordDataSet3.union(wordDataSet4).print()

    //hadoop
    //hadoop
    //hive
    //hive
    //flume
    //spark
    //- - - - - - - - - - - - - - - - - - - - - - - - -
    //hadoop
    //hadoop
  }
}
