package com.czxy.batch.transfromation


import org.apache.flink.api.scala._
/*
 * @Author: Alice菌
 * @Date: 2020/7/26 23:20
 * @Description: 
    
 */
object BatchReduceDemo {
  def main(args: Array[String]): Unit = {

    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    val testDataSet: DataSet[(String, Int)] = env.fromCollection(List(("java" , 1),("java", 1),("java" , 1)))
    
    val groupedDataSet: GroupedDataSet[(String, Int)] = testDataSet.groupBy(0)
    
    val reduceDataSet: DataSet[(String, Int)] = groupedDataSet.reduce((v1,v2)=>(v1._1,v1._2+v2._2))

    reduceDataSet.print()
    // (java,3)

  }
}
