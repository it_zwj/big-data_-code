package com.czxy.batch.transfromation

import org.apache.flink.api.scala.{DataSet, ExecutionEnvironment}
import org.apache.flink.api.scala._

/*
 * @Author: Alice菌
 * @Date: 2020/7/29 14:09
 * @Description: 
    
 */
object BatchCrossDemo {
  def main(args: Array[String]): Unit = {

    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    println("============cross1==================")
    cross1(env)
    println("============cross2==================")
    cross2(env)

    println("============cross3==================")
    cross3(env)
    println("============crossWithTiny==================")
    crossWithTiny1(env)
    println("============crossWithHuge==================")
    crossWithHuge1(env)


  }

  // 定义一个方法
  def cross1(benv:ExecutionEnvironment): Unit ={

    // 1、定义两个DataSet
    val coords1: DataSet[(Int, Int, Int)] = benv.fromElements((1,4,7),(2,5,8),(3,6,9))
    val coords2: DataSet[(Int, Int, Int)] = benv.fromElements((10,40,70),(20,50,80),(30,60,90))

    // 2、交叉两个DataSet[Coord]
    val result1: CrossDataSet[(Int, Int, Int), (Int, Int, Int)] = coords1.cross(coords2)

    // 3、显示结果
    println(result1.collect)

  }

  def cross2(benv:ExecutionEnvironment): Unit ={

    // 1、 定义case class
    case class Coord(id:Int,x:Int,y:Int)

    // 2、 定义两个DataSet[Coord]
    val coords1: DataSet[Coord] = benv.fromElements(Coord(1,4,7),Coord(2,5,8),Coord(3,6,9))
    val coords2: DataSet[Coord] = benv.fromElements(Coord(10,40,70),Coord(20,50,80),Coord(30,60,90))

    // 3、 交叉两个DataSet[Coord]
    val result1: CrossDataSet[Coord, Coord] = coords1.cross(coords2)

    // 4、 显示结果
    println(result1.collect)

  }

 def cross3(benv:ExecutionEnvironment): Unit ={

   // 1、 定义case class
   case class Coord(id:Int,x:Int,y:Int)

   // 2、 定义两个DataSet[Coord]
   val coords1: DataSet[Coord] = benv.fromElements(Coord(1,4,7),Coord(2,5,8),Coord(3,6,9))
   val coords2: DataSet[Coord] = benv.fromElements(Coord(1,4,7),Coord(2,5,8),Coord(3,6,9))

   // 3、定义两个DataSet[Coord]
   val r: DataSet[(Int, Int, Int)] = coords1.cross(coords2) { (c1, c2) => {

     val dist: Int = (c1.x + c2.x) + (c1.y + c2.y)

     (c1.id, c2.id, dist)
   }
   }

   // 4、显示结果
   println(r.collect)

 }


  def crossWithTiny1(benv: ExecutionEnvironment): Unit ={

    // 1、 定义case class
    case class Coord(id:Int,x:Int,y:Int)

    // 2、 定义两个DataSet[Coord]
    val coords1: DataSet[Coord] = benv.fromElements(Coord(1,4,7),Coord(2,5,8),Coord(3,6,9))
    val coords2: DataSet[Coord] = benv.fromElements(Coord(10,40,70),Coord(20,50,80),Coord(30,60,90))

    // 3、交叉两个DataSet[Coord],暗示第二个输入较小
    // 拿第一个输入的每一个元素和第二个输入的每一个元素进行交叉操作。
    val result1: CrossDataSet[Coord, Coord] = coords1.crossWithTiny(coords2)

    // 4、显示结果
    println(result1.collect)


  }

  def crossWithHuge1(benv: ExecutionEnvironment): Unit = {

    // 1、 定义case class
    case class Coord(id:Int,x:Int,y:Int)

    // 2、 定义两个DataSet[Coord]
    val coords1: DataSet[Coord] = benv.fromElements(Coord(1,4,7),Coord(2,5,8),Coord(3,6,9))
    val coords2: DataSet[Coord] = benv.fromElements(Coord(10,40,70),Coord(20,50,80),Coord(30,60,90))

    // 3、 交叉两个Dataset[Coord],暗示第二个输入较大
    val result1: CrossDataSet[Coord, Coord] = coords1.crossWithHuge(coords2)

    // 4、 显示结果
    println(result1.collect)

  }
}
