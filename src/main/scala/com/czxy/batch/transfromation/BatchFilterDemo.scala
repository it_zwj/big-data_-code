package com.czxy.batch.transfromation

import org.apache.flink.api.scala.ExecutionEnvironment

import org.apache.flink.api.scala._
/*
 * @Author: Alice菌
 * @Date: 2020/7/26 23:16
 * @Description: 
    
 */
object BatchFilterDemo {
  def main(args: Array[String]): Unit = {


    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    val testDataSet: DataSet[String] = env.fromElements("hadoop","hive","spark","flink")

    val filterDataSet: DataSet[String] = testDataSet.filter(x=>x.startsWith("h"))

    filterDataSet.print()

    //hadoop
    //hive
  }
}
