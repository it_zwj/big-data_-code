package com.czxy.batch.transfromation

import org.apache.flink.api.java.aggregation.Aggregations
import org.apache.flink.api.scala._
/*
 * @Author: Alice菌
 * @Date: 2020/7/27 18:47
 * @Description: 
    
 */
object BatchAggregateDemo {

  def main(args: Array[String]): Unit = {

 val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    val textDataSet: DataSet[(String, Int)] = env.fromCollection(List(("java",1),("java",1),("scala" , 1)))

    val grouped: GroupedDataSet[(String, Int)] = textDataSet.groupBy(0)

    val aggDataSet: AggregateDataSet[(String, Int)] = grouped.aggregate(Aggregations.SUM,1)

    aggDataSet.print()
    //(java,2)
    //(scala,1)


  }
}
