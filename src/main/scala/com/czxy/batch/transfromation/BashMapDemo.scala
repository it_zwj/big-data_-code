package com.czxy.batch.transfromation

import org.apache.flink.api.scala._
/*
 * @Author: Alice菌
 * @Date: 2020/7/8 10:16
 * @Description: 
    
 */
object BashMapDemo {

  // 创建样例类，用于封装数据
  case class user(id:Int,name:String)

  def main(args: Array[String]): Unit = {
    // 1、 创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 2、 构建数据集
    val sourceDataSet: DataSet[String] = env.fromElements("1,张三","2,李四","3,王五")
    // 3、 数据转换处理
    val userDataSet: DataSet[user] = sourceDataSet.map(item => {

      val itemsArr: Array[String] = item.split(",")
      user(itemsArr(0).toInt, itemsArr(1))
    })

    // 4、 打印结果
    userDataSet.print()
    //user(1,张三)
    //user(2,李四)
    //user(3,王五)

  }
}
