package com.czxy.batch.source.file

import org.apache.flink.api.scala.{DataSet, ExecutionEnvironment}
import org.apache.flink.configuration.Configuration

/*
 * @Author: Alice菌
 * @Date: 2020/7/9 21:39
 * @Description: 
    
 */
object BatchFromHDFSFileSource {
  def main(args: Array[String]): Unit = {
    // 1、创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 2、从HDFS文件构建数据集
   val hdfsFileSource: DataSet[String] = env.readTextFile("hdfs://node01:8020/test/input/wordcount.txt")
    // 3、输出打印
    hdfsFileSource.print()

    //hbase
    //storm flume storm flume
    //aaa
    //word count count
    //hadoop hadoop hadoop
    //kafka
    //redis
    //hbase
    //redis redis redis
    //hive
    //storm flume
    //kafka
    //hbase
    //spark spark spark
    //storm flume
    //kafka
    //spark spark spark
    //redis storm flume

  }
}
