package com.czxy.batch.source.file

import org.apache.flink.api.scala.{DataSet, ExecutionEnvironment}

/*
 * @Author: Alice菌
 * @Date: 2020/7/9 15:06
 * @Description: 
    
 */
object BatchFromCompressFileSource {

  def main(args: Array[String]): Unit = {
    // 1、创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 2、从压缩文件中构建数据集
    val compressFileSource: DataSet[String] = env.readTextFile("data/input/wordcount.txt.gz")
    // 3、输出打印
    compressFileSource.print()

  }
}
