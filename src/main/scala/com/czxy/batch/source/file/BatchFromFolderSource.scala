package com.czxy.batch.source.file
import org.apache.flink.api.scala.{DataSet, ExecutionEnvironment}
import org.apache.flink.configuration.Configuration
/*
 * @Author: Alice菌
 * @Date: 2020/7/9 21:31
 * @Description: 
    
 */
object BatchFromFolderSource {
  def main(args: Array[String]): Unit = {
    //  1、 创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    //  2、 开启
    val configuration: Configuration = new Configuration()
    configuration.setBoolean("recursive.file.enumeration", true)
    //  3、 根据遍历多级目录 来构建数据集
    val result: DataSet[String] = env.readTextFile("data/input/a").withParameters(configuration)
    result.print()

  }
}
