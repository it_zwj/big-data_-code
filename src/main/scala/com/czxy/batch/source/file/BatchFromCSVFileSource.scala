package com.czxy.batch.source.file

import org.apache.flink.api.scala._
import org.apache.flink.api.scala.ExecutionEnvironment

/*
 * @Author: Alice菌
 * @Date: 2020/7/9 15:10
 * @Description: 
    
 */
object BatchFromCSVFileSource {

  // 定义一个样例类
  case class Subject(id:Int,name:String)

  def main(args: Array[String]): Unit = {
    // 1、 创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 2、 从 csv 文件中构建数据集
    val csvDataSet: DataSet[Subject] = env.readCsvFile[Subject]("data/input/subject.csv")
    // 3、 输出打印
    csvDataSet.print()
    //Subject(1,语文)
    //Subject(4,物理)
    //Subject(2,数学)
    //Subject(6,生物)
    //Subject(5,化学)
    //Subject(3,英语)


  }
}
