package com.czxy.batch.source.file

import org.apache.flink.api.scala.{DataSet, ExecutionEnvironment}

/*
 * @Author: Alice菌
 * @Date: 2020/7/9 22:26
 * @Description: 
    
 */
object BatchFromLocalFileSource {
  def main(args: Array[String]): Unit = {
    // 1、 创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 2、 从本地文件构建数据集
    val localFileSource: DataSet[String] = env.readTextFile("data/input/wordcount.txt")
    // 3、 打印输出
    localFileSource.print()
    //Hive Hive Hive Hive Hive
    //Final Memory Finished at
    //Total time BUILD SUCCESS
    //Final Memory Finished at
    //Final Memory Finished at
    //Total time BUILD SUCCESS
    //Total time BUILD SUCCESS
    //Flink Flink Flink Flink Flink

  }
}
