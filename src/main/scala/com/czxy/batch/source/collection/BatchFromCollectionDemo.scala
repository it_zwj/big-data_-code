package com.czxy.batch.source.collection

import org.apache.flink.api.scala.ExecutionEnvironment

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}

/**
  * 读取集合中的批次数据
  */
object BatchFromCollectionDemo {
  def main(args: Array[String]): Unit = {

    // 获取 flink 执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 导入隐式转换
    import org.apache.flink.api.scala._
    //  0. 用 element 创建DataSet(fromElements)
    val ds0: DataSet[String] = env.fromElements("spark","flink")
    ds0.print()

    // 1. 用 Tuple 创建DataSet(fromElements)
    val ds1: DataSet[(Int, String)] = env.fromElements((1,"spark"),(2,"flink"))
    ds1.print()

    // 2. 用 Array 创建DataSet
    val ds2: DataSet[String] = env.fromCollection(Array("spark","flink"))
    ds2.print()

    // 3. 用 ArrayBuffer 创建DataSet
    val ds3: DataSet[String] = env.fromCollection(ArrayBuffer("spark","flink"))
    ds3.print()

    // 4. 用 List 创建DataSet
    val ds4: DataSet[String] = env.fromCollection(List("spark","flink"))
    ds4.print()

    // 5. 用 ListBuffer 创建DataSet
    val ds5: DataSet[String] = env.fromCollection(ListBuffer("spark","flink"))
    ds5.print()

    // 6. 用 Vector 创建 DataSet
    val ds6: DataSet[String] = env.fromCollection(Vector("spark","flink"))
    ds6.print()

    // 7. 用 Queue 创建DataSet
    val ds7: DataSet[String] = env.fromCollection(mutable.Queue("spark","flink"))
    ds7.print()

    // 8. 用 Stack 创建DataSet
    val ds8: DataSet[String] = env.fromCollection(mutable.Stack("spark","flink"))
    ds8.print()

    // 9. 用 Stream 创建 DataSet (Stream相当于 lazy List,避免在中间过程中生成不必要的集合)
    val ds9: DataSet[String] = env.fromCollection(Stream("spark","flink"))
    ds9.print()

    // 10. 用 Seq 创建 DataSet
    val ds10: DataSet[String] = env.fromCollection(Seq("spark","flink"))
    ds10.print()

    // 11. 用 Set 创建 DataSet
    val ds11: DataSet[String] = env.fromCollection(Set("spark","flink"))
    ds11.print()

    // 12. 用 Iterable创建DataSet
    val ds12: DataSet[String] = env.fromCollection(Iterable("spark","flink"))
    ds12.print()

    // 13. 用 ArraySeq 创建 DataSet
    val ds13: DataSet[String] = env.fromCollection(mutable.ArraySeq("spark","flink"))
    ds13.print()

    // 14. 用 ArrayStack 创建 DataSet
    val ds14: DataSet[String] = env.fromCollection(mutable.ArrayStack("spark","flink"))
    ds14.print()

    // 15. 用 Map 创建 DataSet
    val ds15: DataSet[(Int, String)] = env.fromCollection(Map(1 -> "spark",2 -> "flink"))
    ds15.print()

    // 16. 用 Range 创建 DataSet
    val ds16: DataSet[Int] = env.fromCollection(Range(1,9))
    ds16.print()

    // 17. 用 FromElements 创建 DataSet
    val ds17: DataSet[Long] = env.generateSequence(1,9)
    ds17.print()

  }
}
