package com.czxy.batch.sink

import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.api.scala._
/*
 * @Author: Alice菌
 * @Date: 2020/7/9 15:15
 * @Description: 
    
 */
object BatchSinkCollection {
  def main(args: Array[String]): Unit = {

    // 1、 创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 2、 构建数据集
    val source: DataSet[(Int, String, Double)] = env.fromElements((19, "zhangsan", 178.8),
      (17, "lisi", 168.8),
      (18, "wangwu", 184.8),
      (21, "zhaoliu", 164.8))
    
    // 3、 数据打印
    source.print()
    //(19,zhangsan,178.8)
    //(17,lisi,168.8)
    //(18,wangwu,184.8)
    //(21,zhaoliu,164.8)

    println(source.collect())
    //Buffer((19,zhangsan,178.8), (17,lisi,168.8), (18,wangwu,184.8), (21,zhaoliu,164.8))

    source.printToErr()
    //(19,zhangsan,178.8)
    //(17,lisi,168.8)
    //(18,wangwu,184.8)
    //(21,zhaoliu,164.8)

  }
}
