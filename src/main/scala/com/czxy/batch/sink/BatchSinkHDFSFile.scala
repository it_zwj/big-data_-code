package com.czxy.batch.sink

import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.core.fs.FileSystem.WriteMode
import org.apache.flink.api.scala._

/*
 * @Author: Alice菌
 * @Date: 2020/7/25 23:49
 * @Description: 
    
 */
object BatchSinkHDFSFile {
  def main(args: Array[String]): Unit = {
    // 1、 创建执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment
    // 2、 构建数据集
    val source: DataSet[(Int, String, Double)] = env.fromElements(
      (19, "zhangsan", 178.8),
      (17, "lisi", 168.8),
      (18, "wangwu", 184.8),
      (21, "zhaoliu", 164.8)
    )

    // 保存到本地文件（这里设置了数据覆写并指定了分区数为1）
    source.writeAsText("hdfs://node01:8020/test/output/sinkHDFSFile0708",WriteMode.OVERWRITE).setParallelism(1)
    env.execute(this.getClass.getSimpleName)

  }
}
