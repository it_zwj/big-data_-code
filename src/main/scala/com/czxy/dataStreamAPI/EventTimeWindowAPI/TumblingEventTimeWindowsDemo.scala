package com.czxy.dataStreamAPI.EventTimeWindowAPI

import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala.{DataStream, KeyedStream, StreamExecutionEnvironment, WindowedStream}
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.api.scala._

/*
 * @Author: Alice菌
 * @Date: 2020/8/13 11:13
 * @Description: 


    当使用 EventTimeWindow 时，所有的 Window 在 EventTime 的时间轴上进行划分，

 */
/*** 步骤：
  * 1.创建流处理环境
  * 2.设置EventTime
  * 3.构建数据源
  * 4.设置水印
  * 5.逻辑处理
  * 6.引入滚动窗口TumblingEventTimeWindows
  * 7.聚合操作
  * 8.输出打印
  * 9.执行程序     */
object TumblingEventTimeWindowsDemo {
  def main(args: Array[String]): Unit = {

    // 1、创建流处理环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 2、设置 EventTime
    senv.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    // 3、构建数据源
    // 数据格式:   1000 hello
    val socketSource: DataStream[String] = senv.socketTextStream("node01",9999)
    // 4、设置水印
    val groupKeyedStream: KeyedStream[(String, Int), Tuple] = socketSource.assignTimestampsAndWatermarks(
      new BoundedOutOfOrdernessTimestampExtractor[String](Time.seconds(2)) {
        override def extractTimestamp(element: String): Long = {

          // EventTime 是日志生成时间，我们从日志中解析 EventTime
          val eventTime: Long = element.split(" ")(0).toLong
          eventTime
        }
      })

      // 5、逻辑处理
      .map(x => x.split(" ")(1))
      .map((_, 1))
      .keyBy(0)

    // 6、引入滚动窗口TumblingEventTimeWindows
    val windowStream: WindowedStream[(String, Int), Tuple, TimeWindow] = groupKeyedStream.window(TumblingEventTimeWindows.of(Time.seconds(2)))

    // 7、聚合操作
    val resultDataStream: DataStream[(String, Int)] = windowStream.reduce((v1,v2) => (v1._1,v1._2+v2._2))

    // 8、输出打印
    resultDataStream.print()

    // 9、执行程序
    senv.execute("TumblingEventTimeWindowsDemo")


    /**
      * 结果是按照 Event Time 的时间窗口计算得出的，而无关系统的时间（包括输入的快慢）
      */

  }
}
