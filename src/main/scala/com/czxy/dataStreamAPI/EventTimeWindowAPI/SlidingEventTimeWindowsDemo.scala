package com.czxy.dataStreamAPI.EventTimeWindowAPI

import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala.{DataStream, KeyedStream, StreamExecutionEnvironment, WindowedStream}
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.api.scala._
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
/*
 * @Author: Alice菌
 * @Date: 2020/10/24 18:28
 * @Description: 
      滑动窗口（SlidingEventTimeWindows）
*/
/**
  步骤：

  * 1.创建流处理环境
  * 2.设置EventTime
  * 3.构建数据源
  * 4.设置水印
  * 5.逻辑处理
  * 6.引入滑动窗口 SlidingEventTimeWindows
  * 7.聚合操作
  * 8.输出打印
  * 9.执行程序
  */
object SlidingEventTimeWindowsDemo {
  def main(args: Array[String]): Unit = {

    // 1、 创建流处理环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 2、 追加时间特征
    senv.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    // 3、 创建数据源
    val socketSource: DataStream[String] = senv.socketTextStream("node01", 9999)
    // 4、 添加水印
    // 模拟的数据格式： 10000 hello
    val waterMarkDataStream: DataStream[String] = socketSource.assignTimestampsAndWatermarks(

      new BoundedOutOfOrdernessTimestampExtractor[String](Time.seconds(0)) {

        override def extractTimestamp(element: String): Long = {

          val eventTime: Long = element.split(" ")(0).toLong
          eventTime
        }
      })

    // 5、数据处理
    val resultKeyedStream: KeyedStream[(String, Int), Tuple] = waterMarkDataStream
      .map(x => x.split(" ")(1))
      .map((_, 1))
      .keyBy(0)


    // 6、引入滑动窗口 SlidingEventTimeWindows
    val slidingWindowedStream: WindowedStream[(String, Int), Tuple, TimeWindow] = resultKeyedStream.window(SlidingEventTimeWindows.of(Time.seconds(5),
      Time.seconds(2)))

    // 7、聚合计算
    val result: DataStream[(String, Int)] = slidingWindowedStream.reduce((v1,v2) => (v1._1,v1._2+v2._2))

    // 8、打印测试输出
    result.print()

    // 9、执行程序
    senv.execute("SlidingEventTimeWindowsDemo")

  }
  }

