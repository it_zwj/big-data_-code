package com.czxy.dataStreamAPI.EventTimeWindowAPI

import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala.{DataStream, KeyedStream, StreamExecutionEnvironment, WindowedStream}
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.api.scala._
import org.apache.flink.streaming.api.windowing.assigners.EventTimeSessionWindows
import org.apache.flink.streaming.api.windowing.windows.TimeWindow

/*
 * @Author: Alice菌
 * @Date: 2020/10/24 20:58
 * @Description: 
    会话窗口

    相邻两次数据的 EventTime 的时间差超过指定的时间间隔就会触发执行

 */
object EventTimeSessionWindowsDemo {
  def main(args: Array[String]): Unit = {
    // 1、 创建流处理环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 2、 追加时间特征
    senv.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    // 3、 创建数据源
    val socketSource: DataStream[String] = senv.socketTextStream("node01",9999)
    // 4、 添加水印
    // 模拟的数据格式：    1000 hello
    val waterMarkDataStream: DataStream[String] = socketSource.assignTimestampsAndWatermarks(

      new BoundedOutOfOrdernessTimestampExtractor[String](Time.seconds(0)) {
        // 提取时间
        override def extractTimestamp(element: String): Long = {

          val eventTime: Long = element.split(" ")(0).toLong
          eventTime
        }
      })

    // 5、数据处理
    val groupKeyStream: KeyedStream[(String, Int), Tuple] = waterMarkDataStream
      .map(x => x.split(" ")(1))
      .map((_, 1))
      .keyBy(0)

    // 6、 引入会话窗口 EventTimeSessionWindows
    val sessionWindowStream: WindowedStream[(String, Int), Tuple, TimeWindow] = groupKeyStream
      .window(EventTimeSessionWindows.withGap(Time.seconds(5)))

    // 7、 聚合计算
    val resultDataStream: DataStream[(String, Int)] = sessionWindowStream.reduce((v1,v2) => (v1._1,v1._2 + v2._2))

    // 8、 输出打印
    resultDataStream.print()

    // 9、 执行程序
    senv.execute(this.getClass.getSimpleName)


  }
}
