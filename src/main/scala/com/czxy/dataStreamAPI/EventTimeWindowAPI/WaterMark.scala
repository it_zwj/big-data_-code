package com.czxy.dataStreamAPI.EventTimeWindowAPI

import org.apache.flink.shaded.netty4.io.netty.handler.codec.http2.Http2Exception.StreamException
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.streaming.api.windowing.time.Time

/*
 * @Author: Alice菌
 * @Date: 2020/8/12 16:55
 * @Description: 
    
 */
object WaterMark {
  def main(args: Array[String]): Unit = {

    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    // 从调用时刻开始给env创建的每一个stream追加时间特性
    senv.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    val stream: DataStream[String] = senv.readTextFile("eventTest.txt").assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[String](Time.milliseconds(200)) {

      override def extractTimestamp(t: String): Long = {
        // EventTime 是日志生成时间，我们从日志中解析 EventTime
        t.split(" ")(0).toLong
      }

    })

  }
}
