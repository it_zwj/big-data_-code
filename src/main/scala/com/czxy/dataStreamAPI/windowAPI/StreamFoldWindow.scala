package com.czxy.dataStreamAPI.windowAPI

import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow

/*
 * @Author: Alice菌
 * @Date: 2020/8/11 10:31
 * @Description:

 */
object StreamFoldWindow {
  def main(args: Array[String]): Unit = {
    // 1、获取执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 创建 SocketSource
    val stream: DataStream[String] = senv.socketTextStream("node01",9999)
    // 对 stream 进行处理并按 key 聚合
    val streamKeyBy: KeyedStream[(String, Int), Tuple] = stream.flatMap(x => x.split(" ")).map((_,1)).keyBy(0)
    // 引入滚动窗口
    val streamWindow: WindowedStream[(String, Int), Tuple, TimeWindow] = streamKeyBy.timeWindow(Time.seconds(3))
    // 执行 fold 操作
    val streamFold: DataStream[Int] = streamWindow.fold(100) {
      (begin, item) => begin + item._2
    }

    // 将聚合数据写入文件
    streamFold.print()
    // 执行程序
    senv.execute("StreamFoldWindow")

    }
}
