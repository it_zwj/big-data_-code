package com.czxy.dataStreamAPI.windowAPI

import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.scala.{DataStream, KeyedStream, StreamExecutionEnvironment, WindowedStream}
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow

/*
 * @Author: Alice菌
 * @Date: 2020/8/11 09:38
 * @Description:

 */
object StreamReduceWindow {
  def main(args: Array[String]): Unit = {
    // 1、 获取执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 创建 SocketSource
    val stream: DataStream[String] = senv.socketTextStream("node01",9999)
    // 对 stream 进行处理并按 key 聚合
    import org.apache.flink.streaming.api.scala._
    val streamKeyBy: KeyedStream[(String, Int), Tuple] = stream.flatMap(x => x.split("0")).map(item => (item,1)).keyBy(0)
    // 引入时间窗口
    val streamWindow: WindowedStream[(String, Int), Tuple, TimeWindow] = streamKeyBy.timeWindow(Time.seconds(5))
    // 执行聚合操作
    val streamReduce: DataStream[(String, Int)] = streamWindow.reduce(
      (item1, item2) => (item1._1, item1._2 + item2._2)
    )

    // 将聚合数据写入文件
    streamReduce.print()
    // 执行程序
    senv.execute("StreamReduceWindow")

  }
}
