package com.czxy.dataStreamAPI.windowAPI

import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow

/*
 * @Author: Alice菌
 * @Date: 2020/8/11 16:21
 * @Description:

 */
object StreamAggregationWindow {
  def main(args: Array[String]): Unit = {
    // 获取执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 创建 SocketSource
    val socketStream: DataStream[String] = senv.socketTextStream("node01",9999)
    // 对 stream 进行处理并按 key 聚合
    val keyByStream: KeyedStream[(String, String), Tuple] = socketStream.map(item => (item.split(" ")(0),item.split(" ")(1))).keyBy(0)
    // 引入滚动窗口
    val streamWindow: WindowedStream[(String, String), Tuple, TimeWindow] = keyByStream.timeWindow(Time.seconds(5))
    // 执行聚合操作
    val streamMax: DataStream[(String, String)] = streamWindow.max(1)
    // 将聚合数据输出
    streamMax.print()
    // 执行程序
    senv.execute("StreamAggregationWindow")

  }
}
