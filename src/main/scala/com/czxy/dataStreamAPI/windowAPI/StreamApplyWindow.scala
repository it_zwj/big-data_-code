package com.czxy.dataStreamAPI.windowAPI

import org.apache.flink.streaming.api.scala.function.RichWindowFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

/*
 * @Author: Alice菌
 * @Date: 2020/8/11 09:58
 * @Description:

    使用 apply 实现单词统计
    apply 方法可以进行一些自定义处理，通过匿名内部类的方法来实现。
    当有一些复杂计算时使用。

 */
object StreamApplyWindow {
  def main(args: Array[String]): Unit = {
    // 1、获取流处理运行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // 2、构建 socket 流数据源，并指定 IP 地址和端口
    val textDataStream: DataStream[String] = senv.socketTextStream("node01",9999).flatMap(_.split(" "))
    // 3、对接收到的数据转换成单词元组
    val wordDataStream: DataStream[(String, Int)] = textDataStream.map((_,1))
    // 4、使用 keyBy 进行分流（分组）
    val groupedDataStream: KeyedStream[(String, Int), String] = wordDataStream.keyBy(_._1)
    // 5、使用 timeWindow 指定窗口的长度(每3秒计算一次)
    val windowDataStream: WindowedStream[(String, Int), String, TimeWindow] = groupedDataStream.timeWindow(Time.seconds(3))
    // 6、实现一个 WindowFunction 匿名内部类
    /*
      @tparam IN The type of the input value.  输入值的类型
      * @tparam OUT The type of the output value.  输出值的类型
      * @tparam KEY The type of the key.   key值的类型
    * @tparam W The type of Window that this window function can be applied on.  可以应用此窗口功能的窗口类型
    */
    val reduceDataStream: DataStream[(String, Int)] = windowDataStream.apply(new RichWindowFunction[(String, Int), (String, Int), String, TimeWindow] {
      // 自定义操作，在apply 方法中实现数据的聚合
      override def apply(key: String, window: TimeWindow, input: Iterable[(String, Int)], out: Collector[(String, Int)]): Unit = {

        val tuple: (String, Int) = input.reduce((t1, t2) => {
          (t1._1, t1._2 + t2._2)
        })

        // 将要返回的数据收集起来，发送回去
        out.collect(tuple)
      }
    })

    // 打印结果
    reduceDataStream.print()
    // 执行程序
    senv.execute("StreamApplyWindow")

  }
}
