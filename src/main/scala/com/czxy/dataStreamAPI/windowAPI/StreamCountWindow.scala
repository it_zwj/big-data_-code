package com.czxy.dataStreamAPI.windowAPI

import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.scala.{DataStream, KeyedStream, StreamExecutionEnvironment, WindowedStream}
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow

/*
 * @Author: Alice菌
 * @Date: 2020/7/10 09:22
 * @Description:

    CountWindow 的 window_size 指的是相同Key的元素的个数，不是输入的所有元素的总数
 */
object StreamCountWindow {
  def main(args: Array[String]): Unit = {

    // 1、创建执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    // 2、 构建数据源 ， 创建 SocketSource
    val socketSource: DataStream[String] = senv.socketTextStream("node01",9999)

    // 3、 对 stream 进行处理并按 key 聚合
    import org.apache.flink.api.scala._
    val keyByStream: KeyedStream[(String, Int), Tuple] = socketSource.flatMap(x=>x.split(" ")).map((_, 1)).keyBy(0)

    // 4、 引入 countWindow 操作
    // 这里的 5 指的是 5 个相同的 key 的元素计算一次
    val streamWindow: WindowedStream[(String, Int), Tuple, GlobalWindow] = keyByStream.countWindow(5)
    // 执行聚合操作
    val reduceStream: DataStream[(String, Int)] = streamWindow.reduce((v1,v2) => (v1._1,v1._2 + v2._2))

    // 将聚合数据输出
    reduceStream.print(this.getClass.getSimpleName)
    // 执行程序
    senv.execute("StreamCountWindow")

  }
}
