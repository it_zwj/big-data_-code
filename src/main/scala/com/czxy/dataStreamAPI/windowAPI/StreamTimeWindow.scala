package com.czxy.dataStreamAPI.windowAPI

import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow

/*
 * @Author: Alice菌
 * @Date: 2020/8/10 23:53
 * @Description:

 */
object StreamTimeWindow {
  def main(args: Array[String]): Unit = {

    //1.获取执行环境
    val senv: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    //2.创建socket链接获取数据
    val socketSource: DataStream[String] = senv.socketTextStream("node01", 9999)

    //3.进行数据转换处理并按 key 聚合
    val keyByStream: KeyedStream[(String, Int), Tuple] = socketSource.flatMap(x => x.split(" ")).map((_, 1)).keyBy(0)

     //4.引入滚动窗口
    val timeWindowStream: WindowedStream[(String, Int), Tuple, TimeWindow] = keyByStream.timeWindow(Time.seconds(5))

     //5.执行聚合操作
    val reduceStream: DataStream[(String, Int)] = timeWindowStream.reduce(
      (item1, item2) => (item1._1, item1._2 + item2._2)
    )

    //6.输出打印数据
    reduceStream.print()
    //7.执行程序
    senv.execute("StreamTimeWindow")

  }
}
